
import java.util.Scanner;

public class TicTacToeApp {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        //greetings 
        System.out.println("\u001B[36m" + "Welcome to the TIC Tac Toe game" + "\u001B[0m");
        System.out.println("player 1's token : X");
        System.out.println("player 2's token : O");

        
        int player = 1;
        Tile playerToken = Tile.X;
        int player1Wins =0;
        int player2Wins =0; 


        boolean replayGame = true;
        while (replayGame == true){
            Board newBoardGame = new Board();
            boolean gameOver = false;
        while (gameOver == false) {

            System.out.print(newBoardGame);

            // get row and column from user
            System.out.println("player " + player + "'s turn select the row please ");
            int row = reader.nextInt();
            System.out.println("player " + player + " select the column please ");
            int column = reader.nextInt();

            //whose turn is it
            if (player == 1) {
                playerToken = Tile.X;
            } else {
                playerToken = Tile.O;
            }

            boolean validPosition = newBoardGame.placeToken(row, column, playerToken);

            //check if correct position chosen
            while (validPosition == false) {
                System.out.print("Enter another row and column please");
                System.out.println(" select the row please ");
                row = reader.nextInt();
                System.out.println(" select the column please ");
                column = reader.nextInt();
                validPosition = newBoardGame.placeToken(row, column, playerToken);

            }
            // check for win or tie 
            boolean BoardIsFull = newBoardGame.checkIfFull();
            boolean PlayerHasWon = newBoardGame.checkIfWinning(playerToken);

            if (PlayerHasWon == true) {
                System.out.println(" player " + player + " has won ");
                System.out.print(newBoardGame);
                gameOver = true;
                if(player == 1){
                    player1Wins ++;
                }else{
                    player2Wins ++;
                }
            } else if (BoardIsFull == true) {
                System.out.println(" It's a tie ");
                System.out.print(newBoardGame);
                gameOver = true;
            } else {
                player += 1;
                if (player > 2) {
                    player = 1;
                }
            }
        }
            // print the total winning
            System.out.println( " player 1 has won " + player1Wins + " games");
            System.out.println( " player 2 has won " + player2Wins + " games");

            //ask for replay
            System.out.println(" Press 1 to play again 2 to quit");
            int replay = reader.nextInt();
            if( replay == 1){
                replayGame = true;
            }else {
                System.out.println(" Game Over bye bye");
                replayGame = false;
            }
        }
    }
         
}


public class Board {

    //fields 
    private Tile[][] grid;
    private final int size = 3;

    // constructor
    public Board() {
        this.grid = new Tile[this.size][this.size];

        for (int index = 0; index < this.size; index++) {
            for (int j = 0; j < this.size; j++) {
                grid[index][j] = Tile.BLANK;
            }
        }

    }

    //toStrig method
    public String toString() {
        String wholeBoard = "";

        for (int index = 0; index < this.grid.length; index++) {
            for (int j = 0; j < this.grid[index].length; j++) {
                wholeBoard += this.grid[index][j].getName() + " | ";
            }
            wholeBoard += "\n" + "_______________________";
            wholeBoard += "\n" ;
            
        }

        return wholeBoard;
    }

    //placeToken
    public boolean placeToken(int row, int col, Tile playerToken) {
        boolean isInRange = false;
        if ((row >= 0 || row <= 2) && (col >= 0 || col <= 2)) {
            isInRange = true;
        }
        if (this.grid[row][col] == Tile.BLANK) {
            this.grid[row][col] = playerToken;
            isInRange = true;
        }else{
            return false;
        }
        return isInRange;
    }

    //checkIfFull ==> checks if there is any blank if yes returns false
    public boolean checkIfFull() {

        for (int index = 0; index < this.size; index++) {
            for (int j = 0; j < this.size; j++) {
                if (this.grid[index][j] == Tile.BLANK) {
                    return false;
                }
            }
        }
        return true;
    }
    //check Rows
    private boolean checkIfWinningHorizontal(Tile playerToken) {

        for (int index = 0; index < this.size; index++) {
            boolean hasWon = true;
            for (int j = 0; j < this.size; j++) {
                if (this.grid[index][j] != playerToken) {
                    hasWon = false;
                }
            }
             if (hasWon == true){
                return true;
            }
         }
            return false;
         }
         
    //check columns
    private boolean checkIfWinningVertical(Tile playerToken) {

        for (int index = 0; index < this.size; index++) {
            boolean hasWon = true;
            for (int j = 0; j < this.size; j++) {
                if (this.grid[j][index] != playerToken) {
                    hasWon = false;
                }
            }
            if (hasWon == true){
                return true;
            }
         }
        return false;
    }
    // diagonlas winning check
    private boolean checkIfWinningDiagonal(Tile playerToken){
        //first diagonal winning check
        for (int index = 0; index < this.size; index++) {
            boolean hasWon = true;
            for (int j = 0; j < this.size; j++){
                if(this.grid[j][j] != playerToken ) {
                    hasWon = false;
                    break;
                }
            }
            if (hasWon == true){
                return true;
            } 
         }
          /*
         00  __  __
         __  11  __
         __  __  22
         */
      // whenever the indexes are the same.
       // second diagonal winning check
        for (int index = 0; index < this.size; index++) {
            boolean hasWon =true;
            int counter = this.size -1;
            for (int j = 0; j < this.size; j++){
                if(this.grid[j][counter] != playerToken ) {
                    hasWon = false;  
                }
                 counter --;
            }
            if (hasWon == true){
                return true;
            }
         }
        return false;
    /*
         __  __  02
         __  11  __
         20 __  __
     */
    // while the first index is incresing the second decreases
    }
    //check winning  
    public boolean checkIfWinning(Tile playerToken) {
        if (checkIfWinningHorizontal(playerToken) == true
                || checkIfWinningVertical(playerToken) == true 
                || checkIfWinningDiagonal(playerToken)== true) {
            return true;
        }
        return false;
    }
}


public enum Tile {

    BLANK("\u001B[36m" + "  _  " + "\u001B[0m"),
    X("\033[0;31m" + "  X  " + "\u001B[0m"),
    O("\033[0;34m" + "  O  " + "\u001B[0m");

    //field
    private final String name;
    //constructor

    private Tile(String name) {
        this.name = name;
    }
    //getter

    public String getName() {
        return this.name;
    }
}
